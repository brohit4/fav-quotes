

export class QuotesService {
    favoriteQuotes:any[] = [];

    addQuoteToFavorites(quote:any) {
        this.favoriteQuotes.push(quote);
    }

    removeQuoteFromFavorites(quote: any){
        let index = this.favoriteQuotes.findIndex((favQuote) => {
            return favQuote.id === quote.id;
        });
        this.favoriteQuotes.splice(index, 1);
    }

    getFavoriteQuotes(){
        return this.favoriteQuotes;
    }
}