import { Component } from '@angular/core';
import { IonicPage, ModalController} from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import { QuotePage } from '../quote/quote';
import { SettingsService } from '../../services/settings.service';

/**
 * Generated class for the FavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
})
export class FavoritePage {
  quotes: any[] = [];
  constructor(private quotesService: QuotesService, private modalCtrl: ModalController,
  private settingsService: SettingsService) {
  }

  ionViewWillEnter() {
   this.quotes = this.quotesService.getFavoriteQuotes();
  }

  onViewQuote(quote) {
    let modal = this.modalCtrl.create(QuotePage, quote);
    modal.present();
    modal.onDidDismiss((remove) => {
      if (remove) {
        this.quotesService.removeQuoteFromFavorites(quote);
      }
    })
  }
  getQuoteBg() {
    return this.settingsService.getAlternativeBackground() ? 'altBackground' : 'quoteBackground';
  }
}
