import { Component } from "@angular/core";
import { FavoritePage } from "../favorite/favorite";
import { LibraryPage } from "../library/library";


@Component({
    selector:'tabs-page',
    template: `
        <ion-tabs>
            <ion-tab [root]="favoritePage" tabTitle="Favorites" tabIcon="star"></ion-tab>
            <ion-tab [root]="libraryPage" tabTitle="Library" tabIcon="book"></ion-tab>
        </ion-tabs>
    `
})
export class TabsPage {
    favoritePage =  FavoritePage;
    libraryPage = LibraryPage;
}