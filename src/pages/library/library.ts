import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import quotes from './../../data/quotes';
import { QuotesPage } from '../quotes/quotes';

/**
 * Generated class for the LibraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-library',
  templateUrl: 'library.html',
})
export class LibraryPage {
    quotesCollection: {
      category: string,
      quotes: any[],
      icon: string
    }[];
    quotesPage = QuotesPage;

    ngOnInit() {
      this.quotesCollection = quotes;
    }
}
