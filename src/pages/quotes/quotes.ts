import { Component } from '@angular/core';
import { IonicPage, NavParams, AlertController } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';

/**
 * Generated class for the QuotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage {
  quoteCategory:any = {} ;
  constructor(private navParams: NavParams, private alertCtrl: AlertController, private quotesService: QuotesService) {
    this.quoteCategory = this.navParams.data;
  }
  onAddToFavorite(quote) {
    const alert = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Are you sure, you want to add?',
      buttons: [
        {
          text: 'Ok, I want to',
          handler: () => {
            this.quotesService.addQuoteToFavorites(quote);
          }
        },
        {
          text: 'No, I will do it later',
          role: 'cancel',
          handler: () => {
            console.log('Cancelleed')
          }
        }
      ]
    });
    alert.present();
  }
}
